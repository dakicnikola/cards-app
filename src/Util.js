export const getAllCards = () => {
  return JSON.parse(window.localStorage.getItem('cards')) || [];
};

export const saveAllCards = (cards) => {
  window.localStorage.setItem('cards', JSON.stringify(cards));
};

export const getCardById = (id) => {
  const cards = getAllCards();
  return cards[id];
};

export const saveCard = (card) => {
  const cards = this.getAllCards();
  cards[card.id] = card;
  saveAllCards(cards);
};

export const getCardDataTemplate = () => ({
  id: 0,
  cardNumber1: "4455",
  cardNumber2: "4455",
  cardNumber3: "4455",
  cardNumber4: "4455",
  userName: 'User Name',
  expirationDate: (new Date()).getTime()
});