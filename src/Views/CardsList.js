import React, { Component } from "react";
import CardPreview from "../Components/CardPreview";
import { getAllCards } from "../Util";


class CardsList extends Component {

  state = {
    cards: []
  };

  /**
   * Load all cards from local storage on init
   */
  componentDidMount() {
    const cards = getAllCards();
    this.setState({ cards })
  }

  redirect = (path) => {
    this.props.history.push(path)
  };

  /**
   * Creates card list item from cardData
   */
  createCardListItem = (cardData) => {
    const { id } = cardData;
    return (
      <li onClick={this.redirect.bind(this, '/cards/' + id + '/edit')} key={'card-' + id}>
        <CardPreview cardData={cardData} />
      </li>
    )

  };

  render() {
    const { cards } = this.state;
    return (
      <div className={'myCardsContainer'}>
        <h2>My cards</h2>
        <ul className={'cardsList'}>
          {
            !!cards.length &&
            cards.map(this.createCardListItem)
          }
          <li onClick={this.redirect.bind(this, '/cards/add')}>
            <div className={'cardPreview newCard'}>
              <span>+</span>
            </div>
          </li>
        </ul>

      </div>
    )

  }
}

export default CardsList;
