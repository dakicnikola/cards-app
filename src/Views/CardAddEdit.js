import React, { Component } from "react";
import CardPreview from "../Components/CardPreview";
import { TextField } from "@material-ui/core";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns'
import Button from "@material-ui/core/Button";
import { getAllCards, getCardById, getCardDataTemplate, saveAllCards } from "../Util";


class CardAddEdit extends Component {

  state = {
    cardData: {
      userName: '',
      cardNumber1: '',
      cardNumber2: '',
      cardNumber3: '',
      cardNumber4: '',
      expirationDate: null
    },
    validity: {
      cardNumberOk: true,
      expDateOk: true,
      buttonDisabled: true
    }
  };

  static defaultProps = {
    mode: 'ADD'
  };

  nameInputRef;
  cardNumberRef1;
  cardNumberRef2;
  cardNumberRef3;
  cardNumberRef4;
  dateInputRef;


  cardDataTemplate = getCardDataTemplate();


  /**
   * Saving refs, card data and validity
   * Setting title
   */
  componentDidMount() {
    const { mode } = this.props;
    let cardData = { ...this.state.cardData };
    let validity = { ...this.state.validity };
    let title;
    const inputFieldRefs = {
      nameInputRef: this.nameInputRef,
      cardNumberRef1: this.cardNumberRef1,
      cardNumberRef2: this.cardNumberRef2,
      cardNumberRef3: this.cardNumberRef3,
      cardNumberRef4: this.cardNumberRef4,
      dateInputRef: this.dateInputRef,
    };
    if (mode === 'ADD') {
      title = 'Add card to account';
    } else if (mode === 'EDIT') {
      title = 'Edit current card';
      const { id } = this.props.match.params;
      cardData = getCardById(id);
      if (!cardData) {
        this.props.history.push('/cards');
        return;
      }
      validity = this.runFullValidation(cardData);
    }
    this.setState({ cardData, inputFieldRefs, title, validity })
  }


  /**
   * Change handler for input fields (except date)
   * All non digit characters are cut off from card number fields, and limited to 4 digit length
   */
  handleChange = (event) => {
    let { name, value } = event.target;
    const cardData = { ...this.state.cardData };
    let nextField = undefined;
    let preventChange = false;
    if (name.startsWith('cardNumber')) {
      value = value.replace(/\D/g, '');
      preventChange = value.length > 4;
      let cardNumberIndex = parseInt(name[name.length - 1]);
      if (value.length === 4 && cardNumberIndex < 4) {
        nextField = this['cardNumberRef' + (cardNumberIndex + 1)];
      }
    }
    cardData[name] = value;
    if (!preventChange) {
      const validity = this.runFullValidation(cardData);
      this.setState({ cardData, validity }, () => {
        if (nextField) {
          nextField.focus();
          nextField.select();
        }
      })
    }
  };

  /**
   * Date change handler
   */
  handleDateChange = (newDate) => {
    const cardData = { ...this.state.cardData };
    const timeStamp = newDate.getTime();
    cardData['expirationDate'] = timeStamp;
    const validity = this.runFullValidation(cardData);
    this.setState({ cardData, validity })
  };

  /**
   *
   */
  handleSave = () => {
    const { mode } = this.props;
    const cards = getAllCards();
    if (mode === 'ADD') {
      cards.push({ ...this.state.cardData, id: cards.length });
    } else if (mode === 'EDIT') {
      const { id } = this.props.match.params;
      cards[id] = { ...this.state.cardData }
    }
    saveAllCards(cards);
    this.props.history.push('/cards');
  };

  onCancel = () => this.props.history.push('/cards');

  runFullValidation = (cardData) => {
    const cardNumberOk = this.runCardNumberValidation(cardData);
    const expDateOk = this.runDateValidation(cardData);

    const buttonDisabled = !(cardNumberOk && expDateOk);

    return {
      cardNumberOk, expDateOk, buttonDisabled
    }
  };

  runDateValidation = (cardData) => {
    const { expirationDate } = cardData;
    const now = new Date();
    return (expirationDate > now.getTime());
  };

  runCardNumberValidation = (cardData) => {
    const { cardNumber1, cardNumber2, cardNumber3, cardNumber4 } = cardData;
    const cardNumbers = [cardNumber1, cardNumber2, cardNumber3, cardNumber4];
    const firstDigit = cardNumber1 && parseInt(cardNumber1[0]);
    const isValid = (firstDigit >= 4 && firstDigit <= 6) && cardNumbers.every((number) => number && number.length === 4);
    return isValid;
  };

  render() {
    const { cardData, validity, title } = this.state;
    const { userName, cardNumber1, cardNumber2, cardNumber3, cardNumber4, expirationDate } = cardData;
    return (
      <>
        <div className='editCardContainer'>
          <h2>{title}</h2>
          <div className="cardPreviewContainer">
            <CardPreview
              cardData={cardData}
              mode={this.props.mode}
              {...this.state.inputFieldRefs} />
          </div>
          <div>
            <div>Name</div>
            <TextField name="userName"
                       inputRef={(ref) => this.nameInputRef = ref}
                       variant="outlined"
                       value={userName}
                       placeholder={this.cardDataTemplate.userName}
                       onChange={this.handleChange}
            />
          </div>
          <div>
            <div>Card Number</div>
            <div className="cardNumberContainer">
              <TextField name="cardNumber1"
                         inputRef={(ref) => this.cardNumberRef1 = ref}
                         variant="outlined"
                         value={cardNumber1}
                         placeholder={this.cardDataTemplate.cardNumber1}
                         error={!validity.cardNumberOk}
                         helperText={!validity.cardNumberOk && 'Wrong card number'}
                         onChange={this.handleChange}
              />
              <TextField name="cardNumber2"
                         inputRef={(ref) => this.cardNumberRef2 = ref}
                         variant="outlined"
                         value={cardNumber2}
                         placeholder={this.cardDataTemplate.cardNumber1}
                         error={!validity.cardNumberOk}
                         onChange={this.handleChange}
              />
              <TextField name="cardNumber3"
                         inputRef={(ref) => this.cardNumberRef3 = ref}
                         variant="outlined"
                         value={cardNumber3}
                         placeholder={this.cardDataTemplate.cardNumber1}
                         error={!validity.cardNumberOk}
                         onChange={this.handleChange}
              />
              <TextField name="cardNumber4"
                         inputRef={(ref) => this.cardNumberRef4 = ref}
                         variant="outlined"
                         value={cardNumber4}
                         placeholder={this.cardDataTemplate.cardNumber1}
                         error={!validity.cardNumberOk}
                         onChange={this.handleChange}
              />
            </div>
          </div>
          <div>
            <div>Expires on</div>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DatePicker
                name='expirationDate'
                inputRef={(ref) => this.dateInputRef = ref}
                views={["year", "month"]}
                helperText={!validity.expDateOk && "Wrong date"}
                error={!validity.expDateOk}
                inputVariant="outlined"
                value={expirationDate}
                onChange={this.handleDateChange}
              />
            </MuiPickersUtilsProvider>
          </div>
          <div className='buttonsContainer'>
            <Button variant="contained" color="primary" disabled={validity.buttonDisabled} onClick={this.handleSave}>
              Save
            </Button>
            <Button variant="contained" color="secondary" onClick={this.onCancel}>
              Cancel
            </Button>
          </div>
        </div>
      </>
    )
  }
}

export default CardAddEdit;
