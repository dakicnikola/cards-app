import React, { Component } from "react";
import { Visa, MasterCard, Discover, Chip } from "../Assets";
import '../MainStyle.scss';

class CardPreview extends Component {

  static defaultProps = {
    mode: 'PREVIEW',
    cardData: {}
  };

  /**
   * Returns card type and css class for logo
   */
  getCardType = () => {
    const { cardNumber1 } = this.props.cardData;
    const firstDigit = cardNumber1 && cardNumber1[0];
    switch (firstDigit) {
      case '4':
        return {
          type: Visa,
          cssClass: 'visaLogo'
        };
      case '5':
        return {
          type: MasterCard,
          cssClass: 'masterCardLogo'
        };
      case '6':
        return {
          type: Discover,
          cssClass: 'discoverLogo'
        };
      default:
        return null;
    }
  };

  /**
   * Focus appropriate text field on click
   */
  focusInputField = (ref) => {
    if (ref) ref.focus();
  };

  /**
   * Renders date in appropriate format
   */
  renderDate = (timeStamp) => {
    const date = new Date(timeStamp);
    const year = date.getFullYear().toString().slice(2);
    let month = date.getMonth() + 1;
    if (month < 10) month = '0' + month;
    return <span>{month}/{year}</span>
  };

  render() {
    const {
      cardNumber1,
      cardNumber2,
      cardNumber3,
      cardNumber4,
      userName,
      expirationDate
    } = this.props.cardData;
    const { nameInputRef, cardNumberRef1, cardNumberRef2, cardNumberRef3, cardNumberRef4, dateInputRef } = this.props;
    const cardType = this.getCardType();

    return (
      <div className={'cardPreview'}>
        <div id={'cardTypeWrapper'}>
          {cardType && <img src={cardType.type} alt="" className={cardType.cssClass} />}
        </div>
        <div id="chipWrapper">
          <img src={Chip} alt="" className={'card-chip'} id={'chipImg'} />
        </div>
        <div id={'cardNumber'}>
          <span onClick={this.focusInputField.bind(this, cardNumberRef1)}>{cardNumber1}</span>
          <span onClick={this.focusInputField.bind(this, cardNumberRef2)}>{cardNumber2}</span>
          <span onClick={this.focusInputField.bind(this, cardNumberRef3)}>{cardNumber3}</span>
          <span onClick={this.focusInputField.bind(this, cardNumberRef4)}>{cardNumber4}</span>
        </div>
        <div id='nameAndDateWrapper'>
          <span id='userName' onClick={this.focusInputField.bind(this, nameInputRef)}>{userName}</span>
          <span id='expDate'
                onClick={this.focusInputField.bind(this, dateInputRef)}>{this.renderDate(expirationDate)}</span>
        </div>
      </div>
    )
  }
}

export default CardPreview;
