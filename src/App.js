import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import CardsList from "./Views/CardsList";
import CardAddEdit from "./Views/CardAddEdit";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/cards/add" render={(routeProps) => <CardAddEdit mode={'ADD'} {...routeProps}/>} />
          <Route exact path="/cards/:id/edit" render={(routeProps) => <CardAddEdit mode={'EDIT'} {...routeProps}/>} />
          <Route exact path="/cards" component={CardsList} />
          <Redirect to="/cards" />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
